package br.com.itau.investimento.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.itau.investimento.models.Termo;

public interface TermoRepository extends JpaRepository<Termo, Integer> {

}
