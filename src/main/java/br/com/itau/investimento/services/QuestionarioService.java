package br.com.itau.investimento.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.investimento.models.ListaPerguntas;
import br.com.itau.investimento.models.ListaRespostas;
import br.com.itau.investimento.models.Mensagem;
import br.com.itau.investimento.models.Questionario;
import br.com.itau.investimento.models.Resposta;
import br.com.itau.investimento.repositories.QuestionarioRepository;

@Service
public class QuestionarioService {

	@Autowired
	QuestionarioRepository questionarioRepository;

	@Autowired
	PerfilInvestidorService perfilInvestidorService;
	
	Mensagem mensagem = new Mensagem();

	Resposta resposta = new Resposta();

	public Iterable<Questionario> buscarTodasPerguntas() {
		return questionarioRepository.findAll();
	}

	public void inserirPergunta(ListaPerguntas listaPerguntas) {
		
		for (int i = 0; i < listaPerguntas.getListaPerguntas().size(); i++) {
			Questionario questionario = new Questionario();
			questionario = listaPerguntas.getListaPerguntas().get(i);
			questionarioRepository.save(questionario);
		}

	}

	public boolean atualizarPergunta(int id, Questionario questionario) {
		Optional<Questionario> questionarioOptional = questionarioRepository.findById(id);

		if (questionarioOptional.isPresent()) {
			questionario = mesclarAtributos(questionario, questionarioOptional.get());

			questionarioRepository.save(questionario);
			return true;
		}

		return false;
	}

	private Questionario mesclarAtributos(Questionario novo, Questionario antigo) {
		if (novo.getPergunta() != null && !novo.getPergunta().isEmpty()) {
			antigo.setPergunta(novo.getPergunta());
		}

		return antigo;
	}

	public boolean removerPergunta(int id) {
		Optional<Questionario> questionarioOptional = questionarioRepository.findById(id);

		if (questionarioOptional.isPresent()) {
			questionarioRepository.delete(questionarioOptional.get());
			return true;
		}

		return false;
	}

	public Mensagem calcularRespostas(int id, ListaRespostas listaRespostas) {
		int resultado = 0;
		double qtdPergunta = 0.0;
		int perfilCliente = 0;

		for (int i = 0; i < listaRespostas.getListaResposta().size(); i++) {

			Resposta resposta = new Resposta();
			resposta = listaRespostas.getListaResposta().get(i);
			resultado = resultado + resposta.getResposta();
		}

		qtdPergunta = questionarioRepository.findAll().size();
		perfilCliente = (int) Math.round(resultado / qtdPergunta);
		perfilInvestidorService.alterarPerfil(id, perfilCliente);
		
		String perfilRetorno = "";
		
		
		switch (perfilCliente) {
		case 1:
			perfilRetorno = "Seu perfil é Conservador.";
			break;
		case 2:
			perfilRetorno = "Seu perfil é Moderado.";
			break;
		case 3:
			perfilRetorno = "Seu perfil é Arrojado.";
			break;
		case 4:
			perfilRetorno = "Seu perfil é Agressivo.";
			break;

		default:
			break;
		}
		String retorno = "Obrigado por responder nosso questionário! "+"\n" + perfilRetorno;
		
		//return retorno;
		mensagem.setMensagem(retorno);
		
		return mensagem;

	}
}
