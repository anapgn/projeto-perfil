package br.com.itau.investimento.models;

import java.util.ArrayList;
import java.util.List;

public class ListaRespostas {

	private List<Resposta> listaResposta = new ArrayList<Resposta>();

	public List<Resposta> getListaResposta() {
		return listaResposta;
	}

	public void setListaResposta(List<Resposta> listaResposta) {
		this.listaResposta = listaResposta;
	}


}
