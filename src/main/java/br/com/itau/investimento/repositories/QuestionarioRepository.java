package br.com.itau.investimento.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.itau.investimento.models.Questionario;


public interface QuestionarioRepository extends JpaRepository<Questionario, Integer> {

}
