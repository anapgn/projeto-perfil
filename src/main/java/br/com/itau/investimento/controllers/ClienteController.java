package br.com.itau.investimento.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.investimento.models.Cliente;
import br.com.itau.investimento.models.ClienteSaida;
import br.com.itau.investimento.services.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

	@Autowired
	ClienteService clienteService;

	@GetMapping("/{id}")
	public ClienteSaida buscarCliente(@PathVariable int id) {
		return clienteService.obterClientePorIdSaida(id);
	}

	@PostMapping("/inserir")
	public void inserirCliente(@RequestBody Cliente cliente) {
		clienteService.inserir(cliente);
	}

	@PutMapping("/{id}/atualizar")
	public void atualizarCliente(@RequestBody Cliente cliente, @PathVariable int id) {
		clienteService.atualizar(cliente, id);
	}
}
