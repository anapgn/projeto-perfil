	package br.com.itau.investimento.services;

import java.time.Month;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.investimento.helpers.GeradorMeses;
import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.models.Produto;
import br.com.itau.investimento.models.Simulacao;
import br.com.itau.investimento.repositories.AplicacaoRepository;
import br.com.itau.investimento.repositories.ProdutoRepository;

@Service
public class SimulacaoService {
	@Autowired
	CatalogoService catalogoService;
	
	@Autowired
	AplicacaoRepository aplicacaoRepository;
	
	@Autowired
	ClienteService clienteService;
	
	@Autowired
	ProdutoRepository produtoRepository;
	
	@Autowired
	CarteiraInvestimentoService carteiraInvestimentoService;
	
//	public int calcular(Aplicacao aplicacao, int idCliente){
////		public List<Simulacao> calcular(Aplicacao aplicacao, int idCliente){
//		
//		Produto produto = aplicacao.getProduto();
//		Optional<Produto> produtoOptional = catalogoService.obterProdutoPorId(produto.getIdProduto());
//		
//		if(!produtoOptional.isPresent()) {
//			return 3;
//		}
//		
//		aplicacao.setProduto(produto);
//		aplicacao.setCliente(clienteService.obterClientePorId(idCliente));
//// verificar o desenquadramento do cliente
//		if(carteiraInvestimentoService.verificarEnquadramento(aplicacao)) {
//			aplicacaoRepository.save(aplicacao);
//			return 0;
//		}else{
//			return 1;
//		}
//		
////		return calcularPorProduto(produtoOptional.get(), aplicacao.getValor(), aplicacao.getMeses());
//	}
	
	private List<Simulacao> calcularPorProduto(Produto produto, double valor, int meses){
		List<Simulacao> simulacoes = new ArrayList<Simulacao>();
		Locale locale = new Locale("pt", "BR");
		GeradorMeses gerador = new GeradorMeses();
		
		for(int i = 0; i < meses; i++) {
			Simulacao simulacao = new Simulacao();
			
			Month mes = gerador.obterProximoMes();
			
			simulacao.setMes(mes.getDisplayName(TextStyle.FULL, locale));
			simulacao.setValor(valor);
			
			valor += valor * produto.getRendimento();
			
			simulacoes.add(simulacao);
		}
		
		return simulacoes;
	}
}
