package br.com.itau.investimento.models;


import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class ClienteTermo {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idTermoAplicacao;

	@NotNull
	@ManyToOne
	@JoinColumn(name="id_cliente")
	private Cliente cliente;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name="id_termo")
	private Termo termo;


	private LocalDate dataTermo;

	public int getIdTermoAplicacao() {
		return idTermoAplicacao;
	}

	public void setIdTermoAplicacao(int idTermoAplicacao) {
		this.idTermoAplicacao = idTermoAplicacao;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Termo getTermo() {
		return termo;
	}

	public void setTermo(Termo termo) {
		this.termo = termo;
	}

	public LocalDate getDataTermo() {
		return dataTermo;
	}

	public void setDataTermo(LocalDate dataTermo) {
		this.dataTermo = dataTermo;
	}

	
}
