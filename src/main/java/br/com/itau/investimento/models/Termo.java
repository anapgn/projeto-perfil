package br.com.itau.investimento.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class Termo {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idTermo;

	@NotBlank
	private String nomeTermo;

	@NotBlank
	private String descricaoTermo;

	public int getIdTermo() {
		return idTermo;
	}

	public void setIdTermo(int idTermo) {
		this.idTermo = idTermo;
	}

	public String getNomeTermo() {
		return nomeTermo;
	}

	public void setNomeTermo(String nomeTermo) {
		this.nomeTermo = nomeTermo;
	}

	public String getDescricaoTermo() {
		return descricaoTermo;
	}

	public void setDescricaoTermo(String descricaoTermo) {
		this.descricaoTermo = descricaoTermo;
	}
	
	

}
