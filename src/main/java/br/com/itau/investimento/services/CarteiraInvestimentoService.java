package br.com.itau.investimento.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.models.AplicacaoSaida;
import br.com.itau.investimento.models.Cliente;
import br.com.itau.investimento.models.PerfilInvestidor;
import br.com.itau.investimento.models.Produto;

@Service
public class CarteiraInvestimentoService {

	@Autowired
	AplicacaoService aplicacaoService;

	@Autowired
	CatalogoService catalogoService;

	@Autowired
	CarteiraInvestimentoService carteiraInvestimentoService;

	@Autowired
	PerfilInvestidorService perfilInvestidorService;

	@Autowired
	ClienteTermoService clienteTermoService;

	public int calcularCarteiraInvestimento(double valor, int idCliente, int risco) {

		double valorTotal = 0;
		double valorTotalRisco = 0;
		double valorFinal = 0;

		Produto produto = new Produto();

		valorTotalRisco += valor * risco;
		valorTotal += valor;

		Iterable<AplicacaoSaida> aplicacoes = aplicacaoService.listarAplicacoes(idCliente);
		for (AplicacaoSaida aplicacaoSaida : aplicacoes) {

			produto = aplicacaoSaida.getProduto();
			Optional<Produto> produtoOptional = catalogoService.obterProdutoPorId(produto.getIdProduto());

			if (!produtoOptional.isPresent()) {
				return 0;
			}
			produto = produtoOptional.get();
			valorTotalRisco += aplicacaoSaida.getValor() * produto.getRiscoProduto();
			valorTotal += aplicacaoSaida.getValor();

		}
		valorFinal = valorTotalRisco / valorTotal;

		return buscarFaixaPerfil(valorFinal);

	}

	private int buscarFaixaPerfil(double valor) {
		if (0 < valor && valor <= 5) {
			return 1;
		} else {
			if (5.01 < valor && valor <= 20) {
				return 2;
			} else {
				if (20.01 < valor && valor <= 35) {
					return 3;
				} else {
					return 4;
				}
			}
		}

	}

	public boolean verificarEnquadramento(double valor, int IdCliente, int risco) {
		int perfilCarteira = 0;
		int perfilCliente = 0;
		int termo = 2;

		if (clienteTermoService.buscarClienteTermo(IdCliente, termo)) {
			return true;
		}

		perfilCarteira = carteiraInvestimentoService.calcularCarteiraInvestimento(valor, IdCliente, risco);
		Optional<PerfilInvestidor> perfilClienteOptional = perfilInvestidorService.consultarPerfilInvestidor(IdCliente);

		if (!perfilClienteOptional.isPresent()) {
			perfilCliente = 0;
		}

		if (perfilCarteira <= perfilCliente) {
			return true;
		} else {
			return false;
		}

	}
}
