package br.com.itau.investimento.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.investimento.helpers.JwtHelper;
import br.com.itau.investimento.models.Cliente;
import br.com.itau.investimento.services.ClienteService;

@RestController
@RequestMapping("/login")
public class LoginController {

	@Autowired
	ClienteService usuarioService;
	
	@PostMapping
	public ResponseEntity fazerLogin(@RequestBody Cliente cliente) {
		Optional<Cliente> usuarioOptional = usuarioService.buscarPorCpfESenha(
				cliente.getCpf(), 
				cliente.getSenha());
		
		if(usuarioOptional.isPresent()) {
			Optional<String> tokenOptional = JwtHelper.gerar(usuarioOptional.get().getIdCliente());
			
			return ResponseEntity.ok(tokenOptional);
		}
		
		return ResponseEntity.status(400).build();
	}
}
