package br.com.itau.investimento.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.models.AplicacaoSaida;
import br.com.itau.investimento.models.Cliente;
import br.com.itau.investimento.services.AplicacaoService;

@RestController
@RequestMapping("/aplicacao")
public class AplicacaoController {

	@Autowired
	AplicacaoService aplicacaoService;
	
	
	@GetMapping("/{id}/listar")
	public Iterable<AplicacaoSaida> listarAplicacoes(@PathVariable int id){
		return aplicacaoService.listarAplicacoes(id);
	}
	
	@PostMapping("/aplicar")
	public ResponseEntity aplicar(@RequestBody Aplicacao aplicacao){
		int resposta = aplicacaoService.calcular(aplicacao);
		
		switch (resposta) {
		case 0: return ResponseEntity.status(200).build();
		case 1:	return ResponseEntity.status(303).body("Redirecionar para assinatur TCQ--> localhost:8080/termo/inserirclientetermo ou questionario --> localhost:8080/questionario/inserir ");
		case 2: return ResponseEntity.status(303).body("Redirecionar para assinatur TCD --> localhost:8080/termo/inserirclientetermo");
		default: return ResponseEntity.status(500).body("Deu merda");
		}
		
	}
}
